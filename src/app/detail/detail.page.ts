import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokedexService } from '../shared/services/pokedex.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  pokeDetail: any;
  paramRoute = '';

  constructor(
    private pokedexService: PokedexService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => {
      this.paramRoute = data.action;
      console.log("param route ", this.paramRoute);
    });

    // GET SELECTED DATA
    this.pokeDetail = JSON.parse(localStorage.getItem('selected-pokemon'));
    console.log("pokedetail ", this.pokeDetail)
    
    this.fetchDataPokemonTypes();
  }

  goBack() {
    if (this.paramRoute === 'favorite') {
      this.router.navigate(['/favorite']);
    } else {
      this.router.navigate(['/home']);
    }
  }

  fetchDataPokemonTypes() {
    this.pokeDetail.dataDetail.types.forEach((element, index) => {
      this.pokedexService.getDetailPokemon(element.type.url).subscribe(res => {
        this.pokeDetail.dataDetail.types[index].type = res;
      });
    });
  }

}
