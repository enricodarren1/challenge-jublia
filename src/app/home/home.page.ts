import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PokedexService } from 'src/app/shared/services/pokedex.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  pokedexInfo: any;
  pokedexDataList: any;
  pokemonType = [
    {
      name: 'Water',
      value: 'water'
    },
    {
      name: 'Rock',
      value: 'rock'
    },
    {
      name: 'Poison',
      value: 'poison'
    },
    {
      name: 'Ice',
      value: 'ice'
    },
    {
      name: 'Grass',
      value: 'grass'
    },
    {
      name: 'Flying',
      value: 'flying'
    },
    {
      name: 'Fighting',
      value: 'fighting'
    },
    {
      name: 'Electric',
      value: 'electric'
    },
    {
      name: 'Dark',
      value: 'dark'
    },
    {
      name: 'Steel',
      value: 'steel'
    },
    {
      name: 'Physic',
      value: 'physic'
    },
    {
      name: 'Normal',
      value: 'normal'
    },
    {
      name: 'Ground',
      value: 'ground'
    },
    {
      name: 'Ghost',
      value: 'ghost'
    },
    {
      name: 'Fire',
      value: 'fire'
    },
    {
      name: 'Fairy',
      value: 'fairy'
    },
    {
      name: 'Dragon',
      value: 'dragon'
    },
    {
      name: 'Bug',
      value: 'bug'
    }
  ]
  selectedType = '';

  constructor(
    private router: Router,
    private pokedexService: PokedexService
  ) { }

  ngOnInit() {
    this.fetchDataPokedex();
  }

  fetchPokemonByType(ev) {
    console.log("selected type ", this.selectedType);
    this.pokedexDataList = [];
    this.pokedexService.getListPokemonByType(this.selectedType).subscribe((res: any) => {
      this.pokedexDataList = [...res.pokemon];
      this.pokedexDataList.forEach((element, index) => {
        this.pokedexService.getDetailPokemon(element.pokemon.url).subscribe(data => {
          this.pokedexDataList[index].dataDetail = data;
        });
      });
    });
  }

  gotoDetail(data) {
    localStorage.setItem('selected-pokemon', JSON.stringify(data));
    this.router.navigate(['/detail/home']);
  }

  gotoFavorite() {
    this.router.navigate(['/favorite']);
  }

  addFavorite(data) {
    let newData = [];
    let dataLocal = [];

    localStorage.getItem('pokemon-favorites') ? dataLocal = JSON.parse(localStorage.getItem('pokemon-favorites')) : dataLocal = [];
    newData = dataLocal;
    newData.push(data);
    localStorage.setItem('pokemon-favorites', JSON.stringify(newData));
  }

  fetchDataPokedex(paramUrl?) {
    this.pokedexService.getListPokemon(paramUrl).subscribe((res: any) => {
      this.pokedexInfo = res;

      // ASIGN DATA OR PUSH DATA IF EXIST
      if (paramUrl == undefined || paramUrl == null) {
        this.pokedexDataList = [...res.results];
      } else {
        this.pokedexDataList.push.apply(this.pokedexDataList, res.results);
      }
      // ASIGN DATA DETAIL
      this.pokedexDataList.forEach((element, index) => {
        this.pokedexService.getDetailPokemon(element.url).subscribe(data => {
          this.pokedexDataList[index].dataDetail = data;
        });
      });
      console.log("pokedex ", this.pokedexDataList);
    });
  }

  loadData(ev) {
    setTimeout(() => {
      this.fetchDataPokedex(this.pokedexInfo.next);
      ev.target.complete();
    }, 500);
  }
}
