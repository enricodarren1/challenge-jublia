import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.page.html',
  styleUrls: ['./favorite.page.scss'],
})
export class FavoritePage implements OnInit {

  pokedexDataList: any = [];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.pokedexDataList = JSON.parse(localStorage.getItem('pokemon-favorites'));
    console.log("pokedexDatalist ", this.pokedexDataList)
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  gotoDetail(data) {
    localStorage.setItem('selected-pokemon', JSON.stringify(data));
    this.router.navigate(['/detail/favorite']);
  }

  unFavorite(index) {
    console.log("index ", index);
    this.pokedexDataList.splice(index, 1);
    localStorage.setItem('pokemon-favorites', JSON.stringify(this.pokedexDataList));
  }

}
