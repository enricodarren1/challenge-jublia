import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  constructor(
    private http: HttpClient
  ) { }

  getListPokemon(url?) {
    let paramUrl;
    url ? paramUrl = url : paramUrl = 'https://pokeapi.co/api/v2/pokemon?limit=12&offset=0';
    return this.http.get(paramUrl);
  }

  getListPokemonByType(type) {
    return this.http.get(`https://pokeapi.co/api/v2/type/${type}`);
  }

  getDetailPokemon(url) {
    return this.http.get(url)
  }
}
